class CreateDinosaurs < ActiveRecord::Migration
  def change
    create_table :dinosaurs do |t|
      t.integer :cage_id
      t.integer :dinosaur_type_id
      t.string :name

      t.timestamps null: false
    end
  end
end
