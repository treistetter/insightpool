class CreateDinosaurTypes < ActiveRecord::Migration
  def change
    create_table :dinosaur_types do |t|
      t.string :name
      t.boolean :is_carnivore

      t.timestamps null: false
    end
  end
end
