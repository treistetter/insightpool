class AddCapacityToCage < ActiveRecord::Migration
  def change
    add_column :cages, :capacity, :integer
    remove_column :cages, :occupancy
  end
end
