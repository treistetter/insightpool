# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

carnivores = ["Tyrannosaurus", "Velociraptor", "Spinosaurus", "Megalosaurus"]
carnivores.each do |carnivore|
  DinosaurType.where(name: carnivore).first_or_create do |dinosaur_type|
    dinosaur_type.name = carnivore
    dinosaur_type.is_carnivore = true
  end  
end

herbivores = ["Brachiosaurus", "Stegosaurus", "Ankylosaurus", "Triceratops"]
herbivores.each do |herbivore|
  DinosaurType.where(name: herbivore).first_or_create do |dinosaur_type|
    dinosaur_type.name = herbivore
    dinosaur_type.is_carnivore = false
  end  
end