Tim Reistetter Sample Project
===============

Models
---------------------

This project contains three models:

*Dinosaurs (dinosaur_type_id, cage_id, name) - Belongs to Cages and Belongs to Dinosaur Types
*Dinosaur Types (name, is_carnivore) - Has many Dinosaurs
*Cages (power_status) - Has many Dinosaurs

Routes
---------------------

Standard Rails RESTFul routes are available for each model via the /api/v1 name space

Seed Data
---------------------

Seed Data is provided to create some dinosaur types.  Run by using rake db:seed

Concurrency
---------------------

Rails 4 is set up to handle concurrency by default.  The major consideration in moving this api to a concurrent enviornment is to ensure that enough database connections are available in the pool. 

Additional Example Code
---------------------

Below is a hotel reservation api that I have been working on.  It will require installing CouchDB for the application to work:

https://bitbucket.org/treistetter/resapi/