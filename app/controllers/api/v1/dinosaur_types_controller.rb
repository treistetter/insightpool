class  Api::V1::DinosaurTypesController <  Api::V1::ApiController
  before_action :set_dinosaur_type, only: [:show, :edit, :update, :destroy]

  # GET /dinosaur_types
  # GET /dinosaur_types.json
  def index
    @dinosaur_types = DinosaurType.all
  end

  # GET /dinosaur_types/1
  # GET /dinosaur_types/1.json
  def show
  end

  # GET /dinosaur_types/new
  def new
    @dinosaur_type = DinosaurType.new
  end

  # GET /dinosaur_types/1/edit
  def edit
  end

  # POST /dinosaur_types
  # POST /dinosaur_types.json
  def create
    @dinosaur_type = DinosaurType.new(dinosaur_type_params)

    respond_to do |format|
      if @dinosaur_type.save
        format.html { redirect_to @dinosaur_type, notice: 'Dinosaur type was successfully created.' }
        format.json { render :show, status: :created, location: @dinosaur_type }
      else
        format.html { render :new }
        format.json { render json: @dinosaur_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dinosaur_types/1
  # PATCH/PUT /dinosaur_types/1.json
  def update
    respond_to do |format|
      if @dinosaur_type.update(dinosaur_type_params)
        format.html { redirect_to @dinosaur_type, notice: 'Dinosaur type was successfully updated.' }
        format.json { render :show, status: :ok, location: @dinosaur_type }
      else
        format.html { render :edit }
        format.json { render json: @dinosaur_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dinosaur_types/1
  # DELETE /dinosaur_types/1.json
  def destroy
    @dinosaur_type.destroy
    respond_to do |format|
      format.html { redirect_to dinosaur_types_url, notice: 'Dinosaur type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dinosaur_type
      @dinosaur_type = DinosaurType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dinosaur_type_params
      params.require(:dinosaur_type).permit(:name, :is_carnivore)
    end
end
