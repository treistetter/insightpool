class Cage < ActiveRecord::Base
  has_many :dinosaurs
  validates :capacity, presence: true
  validates :power_status, inclusion: ["ACTIVE", "DOWN"]
  validate :must_be_active_with_dinosaurs

  def dinosaur_count
    return self.dinosaurs.count
  end
  
  def is_empty?
    self.dinosaur_count == 0
  end
  
  def unique_dinosaur_type_ids
    return self.dinosaurs.pluck(:dinosaur_type_id).uniq
  end
  
  def contains_only_dinosaur_type?(dinosaur_type_id)
    dinosaur_type_ids = self.unique_dinosaur_type_ids
    return false unless dinosaur_type_ids.length == 1
    return dinosaur_type_ids[0] == dinosaur_type_id 
  end
  
  def is_powered_down?
    return self.power_status != "ACTIVE"
  end
  
  def has_carnivores?
    return !DinosaurType.where(id: self.unique_dinosaur_type_ids, is_carnivore: true).first.nil?
  end

  def has_herbivores?
    return !DinosaurType.where(id: self.unique_dinosaur_type_ids, is_carnivore: false).first.nil?
  end  
  
  def must_be_active_with_dinosaurs
    if !self.is_empty? && self.is_powered_down?
      errors.add(:power_status, "can't be powered down while containing dinosaurs")
    end
  end
end
