class Dinosaur < ActiveRecord::Base
  belongs_to :dinosaur_type
  belongs_to :cage
  
  validates :name, presence: true
  validates :dinosaur_type_id, presence: true
  validates :cage_id, presence: true
  validate :is_herbivore_cage_contains_carnivore, if: :dinosaur_type
  validate :is_carnivore_cage_contains_other_carnivores, if: :dinosaur_type
  
  def is_carnivore
    self.dinosaur_type.is_carnivore
  end
  
  def is_herbivore
    !self.is_carnivore
  end
  
  def is_herbivore_cage_contains_carnivore
    if self.is_herbivore && self.cage.has_carnivores?
      errors.add(:cage_id, "can't contain carnivores if the dinosaur is a herbivore")
    end
  end
  
  def is_carnivore_cage_contains_other_carnivores
    if self.is_carnivore && !self.cage.is_empty? && !self.cage.contains_only_dinosaur_type?(self.dinosaur_type_id)
      errors.add(:cage_id, "can't contain carnivores of different species")      
    end
  end
  
end
