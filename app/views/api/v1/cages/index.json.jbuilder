json.array!(@cages) do |cage|
  json.extract! cage, :id, :capacity, :power_status
  json.url api_v1_cage_url(cage, format: :json)
end
