json.array!(@dinosaur_types) do |dinosaur_type|
  json.extract! dinosaur_type, :id, :name, :is_carnivore
  json.url api_v1_dinosaur_type_url(dinosaur_type, format: :json)
end
