json.array!(@dinosaurs) do |dinosaur|
  json.extract! dinosaur, :id, :cage_id, :dinosaur_type_id, :name
  json.url api_v1_dinosaur_url(dinosaur, format: :json)
end
