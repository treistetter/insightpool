# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dinosaur_type do
    sequence :name do |n| 
      "Dinosaur Type #{n}"
    end
    is_carnivore false
  end
end
