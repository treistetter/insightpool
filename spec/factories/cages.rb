# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cage do
    capacity 1
    power_status "DOWN"
  end
end
