# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dinosaur do
    cage_id 1
    dinosaur_type_id 1
    sequence :name do |n| 
      "Dinosaur #{n}"
    end
  end
end
