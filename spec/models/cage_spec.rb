require 'rails_helper'

RSpec.describe Cage, :type => :model do
  describe ".dinosaur_count" do
    it "Should return a count of dinosaurs currently in the cage" do
      cage = FactoryGirl.create(:cage)
      dino1 = FactoryGirl.create(:dinosaur, cage_id: cage.id)
      dino2 = FactoryGirl.create(:dinosaur, cage_id: cage.id)
      dino3 = FactoryGirl.create(:dinosaur, cage_id: cage.id)
      expect(cage.dinosaur_count).to eq(3)
    end
  end
  
  describe ".has_carnivores?" do 
    it "should return true if the cage contains a dinosaur that is a carnivore" do
      cage = FactoryGirl.create(:cage)
      dino_type = FactoryGirl.create(:dinosaur_type, is_carnivore: true )
      dino = FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      expect(cage.has_carnivores?).to eq(true)      
    end
  end
  
  describe ".has_herbivores?" do 
    it "should return true if the cage contains a dinosaur that is a herbavore" do
      cage = FactoryGirl.create(:cage)
      dino_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      dino = FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      expect(cage.has_herbivores?).to eq(true)      
    end
  end
  
  describe ".is_powered_down?" do
    it "should return false if the power status is ACTIVE" do
      cage = FactoryGirl.build(:cage, power_status: "ACTIVE")
      expect(cage.is_powered_down?).to eq(false)      
    end

    it "should return false if the power status is not ACTIVE" do
      cage = FactoryGirl.build(:cage, power_status: "DOWN")
      expect(cage.is_powered_down?).to eq(true)
      cage.power_status = "invalid"      
      expect(cage.is_powered_down?).to eq(true)
      cage.power_status = "DOWN"      
      expect(cage.is_powered_down?).to eq(true)
    end        
  end

  describe ".unique_dinosaur_type_ids" do 
    it "should return an unique array of species in the cage" do
      cage = FactoryGirl.create(:cage)
      dino_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      dino = FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      dino_type2 = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      dino2 = FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type2.id)
      dino_type3 = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      dino3 = FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type3.id)      
      expect(cage.unique_dinosaur_type_ids).to eq([dino_type.id, dino_type2.id, dino_type3.id])      
    end
  end

  describe ".contains_only_dinosaur_type?(dinosaur_type_id)" do 
    it "should return true if the cage contains only the single dinosaur_type" do
      cage = FactoryGirl.create(:cage)
      dino_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)      
      expect(cage.contains_only_dinosaur_type?(dino_type.id)).to eq(true)      
    end

    it "should return false if the cage contains multiple dinosaur_types" do
      cage = FactoryGirl.create(:cage)
      dino_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type.id)
      dino_type2 = FactoryGirl.create(:dinosaur_type, is_carnivore: false )
      FactoryGirl.create(:dinosaur, cage_id: cage.id, dinosaur_type_id: dino_type2.id)      
      expect(cage.contains_only_dinosaur_type?(dino_type.id)).to eq(false)      
    end

  end
    
  ## Validations 
  
  it "Should not be valid with a blank capacity" do
    cage = FactoryGirl.build(:cage)
    cage.capacity = nil
    expect(cage).to_not be_valid
  end
  

  it "Should not be valid if the power status is not ACTIVE or DOWN" do
    cage = FactoryGirl.build(:cage)
    cage.power_status = "active"
    expect(cage).to_not be_valid
    cage.power_status = "down"
    expect(cage).to_not be_valid
    cage.power_status = "invalid"
    expect(cage).to_not be_valid            
  end
  
  it "Should be valid if the power status is ACTIVE" do
    cage = FactoryGirl.build(:cage)
    cage.power_status = "ACTIVE"
    expect(cage).to be_valid  
  end
  
  it "Should be valid if the power status is DOWN" do
    cage = FactoryGirl.build(:cage)
    cage.power_status = "DOWN"
    expect(cage).to be_valid    
  end
  
  it "Should not be valid if it contains dinosaus and the power is not ACTIVE" do
    cage = FactoryGirl.create(:cage)
    dino1 = FactoryGirl.create(:dinosaur, cage_id: cage.id)
    cage.power_status = "DOWN"
    expect(cage).to_not be_valid  
  end
end
