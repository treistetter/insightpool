require 'rails_helper'

RSpec.describe Dinosaur, :type => :model do
  
  ## Validations
  
  before(:each) do
    @cage = FactoryGirl.create(:cage)
    @dinosaur_type = FactoryGirl.create(:dinosaur_type, is_carnivore: true)
    @dino = FactoryGirl.build(:dinosaur, cage_id: @cage.id, dinosaur_type_id: @dinosaur_type.id)
  end
  
  it "should be valid given valid params" do    
    expect(@dino).to be_valid     
  end
  
  it "should not be valid with a blank name" do
    @dino.name = nil
    expect(@dino).to_not be_valid
  end
  
  it "should not be valid with no dinosaur_type" do
    @dino.dinosaur_type = nil
    expect(@dino).to_not be_valid
  end  

  it "should not be valid if the dino is a herbavore and the cage has carnivores" do
    @dino.save
    expect(@cage.has_carnivores?).to eq(true)
    dinosaur_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false)
    dino = FactoryGirl.build(:dinosaur, cage_id: @cage.id, dinosaur_type_id: dinosaur_type.id) 
    expect(dino).to_not be_valid   
  end
  
  it "should not be valid if the dino is a carnivore and cage contains a different species" do
    @dino.save
    expect(@cage.contains_only_dinosaur_type?(@dinosaur_type.id)).to eq(true)
    dinosaur_type = FactoryGirl.create(:dinosaur_type, is_carnivore: true)
    dino = FactoryGirl.build(:dinosaur, cage_id: @cage.id, dinosaur_type_id: dinosaur_type.id) 
    expect(@cage.contains_only_dinosaur_type?(dinosaur_type.id)).to eq(false)
    expect(dino).to_not be_valid   
  end  

  it "should be valid if the dino is a herbavoire and the cage contains multiple herbavoire species" do
    dinosaur_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false)
    dino = FactoryGirl.create(:dinosaur, cage_id: @cage.id, dinosaur_type_id: dinosaur_type.id)   
    dinosaur_type2 = FactoryGirl.create(:dinosaur_type, is_carnivore: false)
    dino2 = FactoryGirl.create(:dinosaur, cage_id: @cage.id, dinosaur_type_id: dinosaur_type2.id) 
    dinosaur_type = FactoryGirl.create(:dinosaur_type, is_carnivore: false)
    dino = FactoryGirl.build(:dinosaur, cage_id: @cage.id, dinosaur_type_id: dinosaur_type.id)
    expect(dino).to be_valid     
  end
end
