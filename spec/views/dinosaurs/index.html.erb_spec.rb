require 'rails_helper'

RSpec.describe "dinosaurs/index", :type => :view do
  before(:each) do
    assign(:dinosaurs, [
      Dinosaur.create!(
        :cage_id => 1,
        :dinosaur_type_id => 2,
        :name => "Name"
      ),
      Dinosaur.create!(
        :cage_id => 1,
        :dinosaur_type_id => 2,
        :name => "Name"
      )
    ])
  end

  it "renders a list of dinosaurs" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
