require 'rails_helper'

RSpec.describe "dinosaurs/show", :type => :view do
  before(:each) do
    @dinosaur = assign(:dinosaur, Dinosaur.create!(
      :cage_id => 1,
      :dinosaur_type_id => 2,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Name/)
  end
end
