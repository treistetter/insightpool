require 'rails_helper'

RSpec.describe "dinosaurs/edit", :type => :view do
  before(:each) do
    @dinosaur = assign(:dinosaur, Dinosaur.create!(
      :cage_id => 1,
      :dinosaur_type_id => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit dinosaur form" do
    render

    assert_select "form[action=?][method=?]", dinosaur_path(@dinosaur), "post" do

      assert_select "input#dinosaur_cage_id[name=?]", "dinosaur[cage_id]"

      assert_select "input#dinosaur_dinosaur_type_id[name=?]", "dinosaur[dinosaur_type_id]"

      assert_select "input#dinosaur_name[name=?]", "dinosaur[name]"
    end
  end
end
