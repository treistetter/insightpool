require 'rails_helper'

RSpec.describe "dinosaurs/new", :type => :view do
  before(:each) do
    assign(:dinosaur, Dinosaur.new(
      :cage_id => 1,
      :dinosaur_type_id => 1,
      :name => "MyString"
    ))
  end

  it "renders new dinosaur form" do
    render

    assert_select "form[action=?][method=?]", dinosaurs_path, "post" do

      assert_select "input#dinosaur_cage_id[name=?]", "dinosaur[cage_id]"

      assert_select "input#dinosaur_dinosaur_type_id[name=?]", "dinosaur[dinosaur_type_id]"

      assert_select "input#dinosaur_name[name=?]", "dinosaur[name]"
    end
  end
end
