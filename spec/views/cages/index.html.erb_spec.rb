require 'rails_helper'

RSpec.describe "cages/index", :type => :view do
  before(:each) do
    assign(:cages, [
      Cage.create!(
        :occupancy => 1,
        :power_status => "Power Status"
      ),
      Cage.create!(
        :occupancy => 1,
        :power_status => "Power Status"
      )
    ])
  end

  it "renders a list of cages" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Power Status".to_s, :count => 2
  end
end
