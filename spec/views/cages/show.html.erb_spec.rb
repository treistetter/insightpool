require 'rails_helper'

RSpec.describe "cages/show", :type => :view do
  before(:each) do
    @cage = assign(:cage, Cage.create!(
      :occupancy => 1,
      :power_status => "Power Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Power Status/)
  end
end
