require 'rails_helper'

RSpec.describe "cages/new", :type => :view do
  before(:each) do
    assign(:cage, Cage.new(
      :occupancy => 1,
      :power_status => "MyString"
    ))
  end

  it "renders new cage form" do
    render

    assert_select "form[action=?][method=?]", cages_path, "post" do

      assert_select "input#cage_occupancy[name=?]", "cage[occupancy]"

      assert_select "input#cage_power_status[name=?]", "cage[power_status]"
    end
  end
end
