require 'rails_helper'

RSpec.describe "cages/edit", :type => :view do
  before(:each) do
    @cage = assign(:cage, Cage.create!(
      :occupancy => 1,
      :power_status => "MyString"
    ))
  end

  it "renders the edit cage form" do
    render

    assert_select "form[action=?][method=?]", cage_path(@cage), "post" do

      assert_select "input#cage_occupancy[name=?]", "cage[occupancy]"

      assert_select "input#cage_power_status[name=?]", "cage[power_status]"
    end
  end
end
