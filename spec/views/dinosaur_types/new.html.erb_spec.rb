require 'rails_helper'

RSpec.describe "dinosaur_types/new", :type => :view do
  before(:each) do
    assign(:dinosaur_type, DinosaurType.new(
      :name => "MyString",
      :is_carnivore => false
    ))
  end

  it "renders new dinosaur_type form" do
    render

    assert_select "form[action=?][method=?]", dinosaur_types_path, "post" do

      assert_select "input#dinosaur_type_name[name=?]", "dinosaur_type[name]"

      assert_select "input#dinosaur_type_is_carnivore[name=?]", "dinosaur_type[is_carnivore]"
    end
  end
end
