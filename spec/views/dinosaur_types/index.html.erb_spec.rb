require 'rails_helper'

RSpec.describe "dinosaur_types/index", :type => :view do
  before(:each) do
    assign(:dinosaur_types, [
      DinosaurType.create!(
        :name => "Name",
        :is_carnivore => false
      ),
      DinosaurType.create!(
        :name => "Name",
        :is_carnivore => false
      )
    ])
  end

  it "renders a list of dinosaur_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
