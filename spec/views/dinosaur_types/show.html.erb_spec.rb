require 'rails_helper'

RSpec.describe "dinosaur_types/show", :type => :view do
  before(:each) do
    @dinosaur_type = assign(:dinosaur_type, DinosaurType.create!(
      :name => "Name",
      :is_carnivore => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/false/)
  end
end
