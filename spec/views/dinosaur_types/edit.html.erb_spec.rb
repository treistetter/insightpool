require 'rails_helper'

RSpec.describe "dinosaur_types/edit", :type => :view do
  before(:each) do
    @dinosaur_type = assign(:dinosaur_type, DinosaurType.create!(
      :name => "MyString",
      :is_carnivore => false
    ))
  end

  it "renders the edit dinosaur_type form" do
    render

    assert_select "form[action=?][method=?]", dinosaur_type_path(@dinosaur_type), "post" do

      assert_select "input#dinosaur_type_name[name=?]", "dinosaur_type[name]"

      assert_select "input#dinosaur_type_is_carnivore[name=?]", "dinosaur_type[is_carnivore]"
    end
  end
end
